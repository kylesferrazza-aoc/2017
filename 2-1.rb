#!/bin/env ruby

require "test/unit"

class TestAdvent < Test::Unit::TestCase
  def test_simple
    assert_equal 18, checksum("5 1 9 5\n7 5 3\n2 4 6 8")
    assert_equal 409, checksum("5 23 256 391\n14 0\n1 10")
  end
end

def checksum(ins)
  rows = ins.split "\n"
  sum = 0
  for row in rows
    introw = row.split(/\s+/).map {|d| d.to_i}
    max = 0
    min = introw[0]
    for d in introw
      if d > max
        max = d
      elsif d < min
        min = d
      end
    end
    sum += max - min
  end
  sum
end

