#!/bin/env ruby

require "test/unit"

class TestAdvent < Test::Unit::TestCase
  def test_simple
    assert_equal checksum("5 9 2 8\n9 4 7 3\n3 8 6 5"), 9
  end
end

def checksum(ins)
  rows = ins.split "\n"
  sum = 0
  for row in rows
    introw = row.split(/\s+/).map {|d| d.to_i}
    rowsum = 0
    for u in introw
      for v in introw
        if (u % v == 0 and u != v)
          rowsum = u / v
        end
      end
    end
    sum += rowsum
  end
  sum
end
