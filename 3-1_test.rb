#!/bin/env ruby

require "test/unit"

class TestAdvent < Test::Unit::TestCase
  def test_simple
    assert_equal spiralsteps(1), 0
    assert_equal spiralsteps(12), 3
    assert_equal spiralsteps(23), 2
    assert_equal spiralsteps(1024), 31
  end
end

def spiralsteps(destination)
  if (1..8).member? destination then
    return destination.odd? ? 1 : 2
  end
end

a = [[36, 35, 34, 33, 32, 31, 30],
     [37, 16, 15, 14, 13, 12, 29],
     [38, 17,  4,  3,  2, 11, 28],
     [39, 18,  5,  0,  1, 10, 27],
     [40, 19,  6,  7,  8,  9, 26],
     [41, 20, 21, 22, 23, 24, 25],
     [42, 43, 44, 45, 46, 47, 48]]

