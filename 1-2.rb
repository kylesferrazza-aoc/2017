#!/bin/env ruby

require "test/unit"

class TestAdvent < Test::Unit::TestCase
  def test_simple
    assert_equal 6, sum("1212")
    assert_equal 0, sum("1221")
    assert_equal 4, sum("123425")
    assert_equal 12, sum("123123")
    assert_equal 4, sum("12131415")
  end
end

def sum(ins)
  add_num = ins.length / 2
  check_me = ins + ins[0..add_num]
  sum = 0
  for i in 0..(check_me.length-1)
    if (check_me[i] == check_me[i + add_num])
      sum += ins[i].to_i
    end
  end
  sum
end
