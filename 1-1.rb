#!/bin/env ruby

require "test/unit"

class TestAdvent < Test::Unit::TestCase
  def test_simple
    assert_equal 3, sum("1122")
    assert_equal 4, sum("1111")
    assert_equal 0, sum("1234")
    assert_equal 9, sum("91212129")
  end
end

def sum (ins)
  check_me = ins + ins[0]
  sum = 0
  for i in 0..(check_me.length-1)
    if (check_me[i] == check_me[i + 1])
      sum += ins[i].to_i
    end
  end
  sum
end
